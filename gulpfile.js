const { src, dest, watch } = require("gulp");
const sass = require("gulp-sass")(require("sass"));
const browserify = require("browserify");
const babelify = require("babelify");
const source = require("vinyl-source-stream");
const uglify = require("gulp-uglify");
const buffer = require("vinyl-buffer");
const image = require("gulp-imagemin");

function styles() {
	return src("./src/styles/style.scss")
		.pipe(sass({ outputStyle: "compressed" }).on("error", sass.logError))
		.pipe(dest("dist"));
}

function scripts() {
	return browserify("src/main.js")
		.transform(
			babelify.configure({
				presets: ["@babel/preset-env"],
			})
		)
		.bundle()
		.pipe(source("bundle.js"))
		.pipe(buffer())
		.pipe(uglify())
		.pipe(dest("dist"));
}

function images() {
	return src("public/img/*")
		.pipe(image({optimizationLevel: 7}))
		.pipe(dest("dist/img/"));
}

function sentinel() {
	watch("./public/img/*", { ignoreInitial: false }, images);
	watch("./src/**/*.js", { ignoreInitial: false }, scripts);
	watch("./src/styles/**/*.scss", { ignoreInitial: false }, styles);
}

exports.sentinel = sentinel;
