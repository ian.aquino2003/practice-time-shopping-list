export class PointsList {
	constructor() {
		this.turisticPoints = [
		{
			title: 'Pão de Açúcar',
			description: 'Amet minim mollit non deserunt ullamco est sit aliqua dolor dosa amet sint. Velit officia consece duis enim velit mollit.',
			base64: 'dist/img/pao-de-acucar.png'
		},
		{
			title: 'Ilha Grande',
			description: 'Amet minim mollit non deserunt ullamco est sit aliqua dolor dosa amet sint. Velit officia consece duis enim velit mollit.',
			base64: 'dist/img/ilha-grande.png'
		},
		{
			title: 'Cristo Redentor',
			description: 'Amet minim mollit non deserunt ullamco est sit aliqua dolor dosa amet sint. Velit officia consece duis enim velit mollit.',
			base64: 'dist/img/cristo-redentor.png'
		},
		{
			title: 'Centro Histórico de Paraty',
			description: 'Amet minim mollit non deserunt ullamco est sit aliqua dolor dosa amet sint. Velit officia consece duis enim velit mollit.',
			base64: 'dist/img/centro-paraty.png'
		},
		];

		console.log(this.turisticPoints)

		this.selectors();
		this.events();
		this.renderList();
	}

	selectors() {
		this.imageInput = document.querySelector("#inputImage");
		this.titleInput = document.querySelector("#title");
		this.descriptionInput = document.querySelector("#description");
		this.addInput = document.querySelector(".addButton");
		this.elementsContainer = document.querySelector("main");
		this.imgLabel = document.querySelector("label");
		this.uploadDiv = document.querySelector(".upload");
		this.imgBase64 = "";
	}

	events() {
		this.addInput.addEventListener("click", async (evt) => {
			if (
				this.validade(this.imageInput) &&
				this.validade(this.titleInput) &&
				this.validade(this.descriptionInput)
			) {
				this.turisticPoints.push({
					title: this.titleInput.value,
					description: this.descriptionInput.value,
					base64: this.imgBase64,
				})

				this.renderList();
			} else {
				alert("Por favor preencha os campos pra adicionar");
			}
		});

		this.imageInput.addEventListener("change", async (evt) => {
			this.imgLabel.innerText = this.imageInput.files[0].name;
			this.imgBase64 = await this.getBase64(this.imageInput.files[0]);
		});
		this.uploadDiv.addEventListener("click", async (evt) => {
			this.imageInput.click();
		})
	}

	getBase64(file) {
		return new Promise((resolve, reject) => {
			const reader = new FileReader();
			reader.readAsDataURL(file);
			reader.onload = () => resolve(reader.result);
			reader.onerror = (error) => reject(error);
		});
	}

	addElement(object) {
		let container = document.createElement("div");
		let img = document.createElement("img");
		let title = document.createElement("h2");
		let description = document.createElement("p");
		let textcontents = document.createElement("div");
		let removeItem = document.createElement("button");

		img.alt = object.title;
		img.src = object.base64;
		title.innerText = object.title;
		description.innerText = object.description;

		removeItem.classList.add("deleteButton");
		removeItem.innerHTML = "Delete";

		removeItem.addEventListener("click", async () => {
			const idx = this.turisticPoints.findIndex((v,i) => {
				return v.title === object.title
			})
			this.turisticPoints.splice(idx,1)

			this.renderList();
		});

		textcontents.appendChild(title);
		textcontents.appendChild(description);

		container.classList.add("containerElement");

		container.appendChild(img);
		container.appendChild(textcontents);
		container.appendChild(removeItem);
		this.elementsContainer.appendChild(container);
	}

	validade(input) {
		if (!input.value) {
			input.classList.add("error");
			return false;
		} else {
			input.classList.remove("error");
			return true;
		}
	}

	renderList() {
		console.log(this.turisticPoints)
		this.elementsContainer.innerHTML = "";

		if(this.turisticPoints.length <= 0) {
			this.elementsContainer.innerHTML = "Nenhum ponto cadastrado pelos usuários."
			return;
		}

		this.turisticPoints.forEach((point) => {
				this.addElement(point);
		});
	}
}
